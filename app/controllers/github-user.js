import Ember from 'ember';

export default Ember.Controller.extend({
  needs: ['application','home'],
  reposAreCollapsed: false,

  actions: {
    toggleReposCollapse: function toggleCollapse () {
      this.toggleProperty('reposAreCollapsed');
    }
   }
});
