import Ember from 'ember';

export default Ember.Controller.extend({
  needs: ['application'],
  metadataBinding: "controllers.application.responseMeta",
  accountDataBinding: "model",

  actions: {

    fetchIssues: function fetchIssuesAction (repo) {
      var self, username, githubUrl, emberResponse;
      self = this;
      username = self.get('username');
      githubUrl = "https://api.github.com/repos/%@/%@/issues".loc(repo.owner.login,repo.name);
      if (self.get('issueData')) {
        // Show github repo issues
      } else {
        Ember.$.ajax({
          url:  githubUrl,
          type: "GET",
          dataType: 'jsonp',
          success: function githubIssuesSuccessCallback (response) {
            // Wrap response with Ember.Object
            emberResponse = Ember.Object.create(response);
            // Success notification
            self.notify.success('Github for issues for %@ have been fetched.'.loc(repo.name));
            // Set controller properties for use in templates
            self.set('responseMeta', emberResponse.get('meta'));
            self.set('accountData.issues', emberResponse.get('data').toArray());
          },
          error: function githubIssuesFailCallback (response) {
            emberResponse = Ember.Object.create(response);
            if (response.get('responseJSON.message')) {
              self.notify.alert(response.get('responseJSON.message'));
            } else {
              self.notify.alert("Sorry, there was an error with your request.");
            }
          }
        });
      }
    }
   }
});
