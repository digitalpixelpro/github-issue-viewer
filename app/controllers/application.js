import Ember from 'ember';

export default Ember.Controller.extend({

  actions: {
    // resets the Github Username field in the navbar
    clearForm: function clearForm () {
      this.set('username', '');
      Ember.$('#githubName').focus();
    },

    getRepos: function (name) {
      if (!Ember.isEmpty(name)) {
        this.transitionToRoute('home', name);
      }
    }
  }
});
