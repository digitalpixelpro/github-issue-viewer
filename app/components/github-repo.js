import Ember from 'ember';

export default Ember.View.extend(Ember.ViewTargetActionSupport,{
  tagName: "li",
  classNames: "list-group-item clickable",
  templateName: "components/github-repo",
  classNameBindings: "isSelected:active",

  isSelected: function isSelectedProperty () {
    return Ember.isEqual(this.get('parentView.currentRepo'),this.get('content'));
  }.property('parentView.currentRepo'),

  click: function githubRepoClick () {
    this.set('controller.currentRepo', this.get('content'));
    this.get('parentView').set('currentRepo', this.get('content'));
    this.triggerAction({action: 'fetchIssues'});
  }
});