import Ember from 'ember';

export default Ember.Route.extend({
  model: function getRepos (params) {
    // Submits AJAX request to Githubs api for a supplied username
    var githubUrl, self, emberResponse;
    self = this;
    githubUrl = "https://api.github.com/users/%@/repos".loc(params.username);
    return new Ember.RSVP.Promise(function modelPromise (resolve, reject) {
      Ember.$.ajax({
        url:  githubUrl,
        type: "GET",
        dataType: 'jsonp',
        success: function githubRepoSuccessCallback (response) {
          // Wrap response with Ember.Object
          emberResponse = Ember.Object.create(response);
          // Success notification
          self.notify.success('Github for repos for %@ have been fetched.'.loc(params.username));
          // Set controller properties for use in templates
          self.controllerFor('application').setProperties({
            responseMeta: emberResponse.get('meta'),
            lastFetchedUsername: params.username
          });
          resolve({repos: emberResponse.get('data'), issues: []});
        },
        error: function githubRepoFailCallback (response) {
          emberResponse = Ember.Object.create(response);
          if (response.get('responseJSON.message')) {
            self.notify.alert(response.get('responseJSON.message'));
          } else {
            self.notify.alert("Sorry, there was an error with your request.");
          }
          reject(emberResponse);
        }
      });
    });
  }
});
