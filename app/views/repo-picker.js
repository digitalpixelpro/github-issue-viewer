import Ember from 'ember';
import GithubRepo from 'github-issue-viewer/components/github-repo';

export default Ember.CollectionView.extend({
  tagName: "ul",
  classNames: "list-group repo-picker",
  itemViewClass: GithubRepo
});